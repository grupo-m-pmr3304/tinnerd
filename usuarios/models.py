from django.db import models
from django.contrib.auth.models import Group
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin


class AdmUsuario(BaseUserManager):
    
    def get_by_natural_key(self, email_):
        return self.get(email=email_)

    def create_user(self, nome, email, nusp, curso, is_aluno, is_monitor, password=None):
        if not email:
            raise ValueError('Users must have an e-mail')

        user = self.model(
            nome=nome,
            email=email,
            nusp=nusp,
            curso=curso,
            is_aluno=is_aluno,
            is_monitor=is_monitor,
        )
        user.set_password(password)
        user.activated=True
        user.save(using=self._db)

        if is_aluno:
            group = Group.objects.get(name='Aluno')
            user.groups.add(group)

        if is_monitor:
            group = Group.objects.get(name='Monitor')
            user.groups.add(group)    

        return user

    def create_superuser(self, nome, email, nusp, curso, is_aluno, is_monitor, password):
        user = self.create_user(email=email, 
                                nome=nome,
                                nusp=nusp,
                                curso=curso,
                                is_aluno=is_aluno,
                                is_monitor=is_monitor,
                                password=password,
                                )
        user.activated= True
        user.is_admin = True
        user.is_aluno = True
        user.is_monitor = True
        user.save(using=self._db)
        return user

class Usuario(AbstractBaseUser, PermissionsMixin):
    nome = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(unique=True)
    nusp = models.IntegerField(null=True, blank=True)
    curso = models.CharField(max_length=255, null=True, blank=True)
    is_admin = models.BooleanField('admin status',default=False)
    is_aluno = models.BooleanField('Aluno?', default=False)
    is_monitor = models.BooleanField('Monitor?', default=False)

    objects = AdmUsuario()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nome', 'nusp', 'curso', 'is_aluno', 'is_monitor']

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def is_superuser(self):
        return self.is_admin

    def natural_key(self):
        return self.email

    def __str__(self):
        return f'{self.nome}'
