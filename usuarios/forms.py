from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from .models import Usuario

class CadastroForm(UserCreationForm):
    class Meta:
        model = Usuario
        fields = [
            'nome',
            'email',
            'nusp',
            'curso',
            'is_aluno',
            'is_monitor',
        ]
        labels = {
            'nome': 'Nome Completo',
            'email': 'Email Institucional',
            'nusp': 'Número USP',
            'curso': 'Curso',
            'is_aluno':'Aluno?',
            'is_monitor':'Monitor?',
        }