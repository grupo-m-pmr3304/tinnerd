from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('', include('staticpages.urls')),
    path('aplicativo/', include('aplicativo.urls')),
    path("admin/", admin.site.urls),
    path('usuarios/', include('usuarios.urls')),
    path('usuarios/', include('django.contrib.auth.urls')),
]
