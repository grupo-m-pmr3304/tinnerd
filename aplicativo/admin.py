from django.contrib import admin
from .models import Disciplina, Aula, Feedback, Dia

admin.site.register(Disciplina)
admin.site.register(Aula)
admin.site.register(Feedback)
admin.site.register(Dia)