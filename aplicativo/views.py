from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.contrib.auth.decorators import login_required
from .models import Disciplina, Aula, Feedback
from usuarios.models import Usuario
from .forms import DisciplinaForm, FeedbackForm, AulaForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth import get_user_model


@login_required
def create_disciplina(request):
    if request.method == 'POST':
        disciplina_form = DisciplinaForm(request.POST)
        if disciplina_form.is_valid():
            disciplina = Disciplina(**disciplina_form.cleaned_data)
            disciplina.save()
            return HttpResponseRedirect(
                reverse('aplicativo:disciplinas'))
    else:
        disciplina_form = DisciplinaForm()
    context = {'disciplina_form': disciplina_form}
    return render(request, 'aplicativo/add_disciplina.html', context)

class DisciplinaListView(LoginRequiredMixin, generic.ListView):
    model = Disciplina
    template_name = 'aplicativo/lista_disciplinas.html'

class UsuarioDetailView(LoginRequiredMixin, generic.DetailView):
    model = Usuario
    template_name = "aplicativo/detail_perfil.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class UsuarioListView(LoginRequiredMixin, generic.ListView):
    model = Usuario
    template_name = "aplicativo/lista_usuarios.html"

class UsuarioDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Usuario
    template_name = "aplicativo/delete_usuario.html"
    success_url = reverse_lazy('aplicativo:usuarios')

@login_required
def create_aula(request):
    if request.method == 'POST':
        aula_form = AulaForm(request.POST)
        if aula_form.is_valid():
            aula_monitor = request.user
            aula_disciplina = aula_form.cleaned_data['disciplina']
            aula_dia = aula_form.cleaned_data['dia']
            aula_inicio = aula_form.cleaned_data['inicio']
            aula_fim = aula_form.cleaned_data['fim']
            aula = Aula(monitor = aula_monitor,
                        disciplina = aula_disciplina,
                        dia = aula_dia,
                        inicio = aula_inicio,
                        fim = aula_fim)
            aula.save()
            return HttpResponseRedirect(
                reverse('index'))
    else:
        aula_form = AulaForm()
    context = {'aula_form': aula_form}
    return render(request, 'aplicativo/create_aula.html', context)

class AulaListView(LoginRequiredMixin, generic.ListView):
    model = Aula
    template_name = 'aplicativo/lista_aulas.html'

class AulaDetailView(generic.DetailView):
    model = Aula
    template_name = "aplicativo/detail_aula.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

@login_required
def create_feedback(request, aula_id):
    aula = get_object_or_404(Aula, pk=aula_id)
    if request.method == 'POST':
        feedback_form = FeedbackForm(request.POST)
        if feedback_form.is_valid():
            feedback_autor = request.user
            feedback_texto = feedback_form.cleaned_data['texto']
            feedback = Feedback(autor=feedback_autor,
                            texto=feedback_texto,
                            aula=aula)
            feedback.save()
            return HttpResponseRedirect(
                reverse('aplicativo:detail_aula', args=(aula_id, )))
    else:
        feedback_form = FeedbackForm()
    context = {'feedback_form': feedback_form, 'aula': aula}
    return render(request, 'aplicativo/feedback.html', context)

def filtra_disciplinas(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        aula_list = Aula.objects.filter(disciplina__icontains=search_term)
        context = {"aula_list": aula_list}
    return render(request, 'aplicativo/pesquisa.html', context)
