from django.forms import ModelForm
from .models import Disciplina, Aula, Feedback, Dia


class DisciplinaForm(ModelForm):
    class Meta:
        model = Disciplina
        fields = [
            'titulo',
            'codigo',
        ]
        labels = {
            'titulo': 'Nome da Disciplina',
            'codigo': 'Código da Disciplina',
        }

class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        fields = [
            'texto',
        ]
        labels = {
            'texto': 'Comentários',
        }

class AulaForm(ModelForm):
    class Meta:
        model = Aula
        fields = [
            'disciplina',
            'dia',
            'inicio',
            'fim',
        ]
        labels = {
            'disciplina': 'Disciplina',
            'dia': 'Dia da Semana',
            'inicio': 'De:',
            'fim':'Até:',
        }